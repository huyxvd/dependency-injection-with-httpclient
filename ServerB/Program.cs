WebApplicationBuilder builder = WebApplication.CreateBuilder(args);

// Add services to the container.

WebApplication app = builder.Build();

// Configure the HTTP request pipeline.

app.MapGet("/b", () =>
{
    return "hello from server B";
});

app.Run();
